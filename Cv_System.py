import cv2
src = cv2.VideoCapture("C:/Users/Vsb/PycharmProjects/pythonProject/Material/Sample.mp4")
fourcc=cv2.VideoWriter_fourcc(*'XVID')
out=cv2.VideoWriter("gray.avi",fourcc,20.0,(640,480),0)
while True:
    ret, frame = src.read()
    image = cv2.rotate(frame, cv2.cv2.ROTATE_90_CLOCKWISE)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    out.write(gray)
    cv2.imshow("Output",gray)
    if cv2.waitKey(1) & 0xFF == ord('s'):
        break
src.release()
out.release()
cv2.destroyAllWindows()

